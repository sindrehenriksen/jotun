jotun is a basic Flask-based dashboard portal which allows access to different apps to be controlled
for different users. Currently, jotun supports deployment of both Flask and Dash apps behind a login
screen (through the SecureFlask and SecureDash classes). The view is that this might get extended
to other dashboarding technologies (e.g. Tableau server) as the need arises.

jotun.py handles adding users and apps. 