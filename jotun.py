#!/usr/bin/python3
from app.models import User, App
from app import db

from prettytable import PrettyTable

import os.path
import sys
import argparse
from getpass import getpass

def add_user(username, email):
    username = username.lower()
    email = email.lower()
    u = User(username=username, email=email)
    pw1=getpass('Set password: ')
    pw2=getpass('Confirm password: ')
    assert pw1==pw2, 'Error: Passwords must match'
    u.set_password(pw1)
    u.apps = []
    
    db.session.add(u)

    db.session.commit()
    print('User %s created successfully.'%username)

def rm_user(username, force=False):
    username = username.lower()
    confirm = input('Confirm user to delete: ')
    if (username == confirm) or force:
        db.session.query(User).filter_by(username=username).delete()
        db.session.commit()

def add_app(filename, url, description=''):
    a = App(filename=filename, url=url, description=description)
    db.session.add(a)
    db.session.commit()
    print('App %s created successfully.'%filename)

def rm_app(filename, force=False):
    print('Note: this only removes the application from the database. It does not delete the app itself.')
    confirm = input('Confirm application filename to delete: ')
    if (filename == confirm) or force:
        db.session.query(App).filter_by(filename=filename).delete()
        db.session.commit()


def add_permissions(filenames, usernames):
    if type(filenames) != list:
        filenames = [filenames]

    if type(usernames) != list:
        usernames = [usernames]
        
    for filename in filenames:
        for username in usernames:
            
            u = User.query.filter_by(username=username).first()
            a = App.query.filter_by(filename=filename).first()
            u.apps.append(a)
            db.session.add(u)
            
    db.session.commit()

def rm_permissions(filenames, usernames):
    if type(filenames) != list:
        filenames = [filenames]

    if type(usernames) != list:
        usernames = [usernames]
        
    for filename in filenames:
        for username in usernames:
            u = User.query.filter_by(username=username).first()
            a = App.query.filter_by(filename=filename).first()
            u.apps = [k for k in u.apps if k.id != a.id]
            db.session.add(u)

    db.session.commit()


def list_users():
    users = PrettyTable(['User','Email','Apps'])
    for user in User.query.all():
        userApps = '(' + ', '.join([(app.filename or 'NA') for app in user.apps]) + ')'
        users.add_row([user.username, user.email, userApps])
        
    print(users)
    
def list_apps():
    apps = PrettyTable(['Filename','URL','Users', 'Description'])
    for app in App.query.all():
        appUsers = '('+', '.join([user.username for user in app.user]) + ')'
        apps.add_row([app.filename or 'NA',
                      app.url or 'NA',
                      appUsers,
                      app.description])

    print(apps)

    
def main():
    p = argparse.ArgumentParser(description='Tool for managing the Jotun database',
			      usage='jotun [OPTIONS]')

    p.add_argument('--init', action='store_true')
    p.add_argument('--purge', action='store_true')
    p.add_argument('--add-user', nargs='*')
    p.add_argument('--rm-user', nargs=1)
    p.add_argument('--add-app', nargs='*')
    p.add_argument('--rm-app', nargs=1)
    p.add_argument('--add-permissions', nargs=2)
    p.add_argument('--rm-permissions', nargs=2)
    p.add_argument('--list-users', action='store_true')
    p.add_argument('--list-apps', action='store_true')


    args = p.parse_args()

    if args.init:
        db.create_all()

    if args.purge:
        print('Warning! This will purge the existing database.')
        purge = input('Type "PURGE" to continue: ')
        if purge == 'PURGE':
            db.session.execute('drop table if exists user;')
            db.session.execute('drop table if exists app;')
            db.session.execute('drop table if exists app_user;')
            db.session.commit()

            print('Successfully purged database.')
        else:
            print('Cancelling.')

    if args.add_user is not None:
        if len(args.add_user) < 1:
            username=input('User name: ')
        else:
            username = args.add_user[0]
            
        if len(args.add_user) < 2:
            email=input('Email: ')
        else:
            email = args.add_user[1]
        
        add_user(username, email)

    if args.add_app is not None:
        if len(args.add_app) < 1:
            filename = input('App file name (exclude trailing .py): ')

        else:
            filename = args.add_app[0]

        if len(args.add_app) < 2:
            url = input('URL: ')
        else:
            url = args.add_app[1]

        add_app(filename, url)

    if args.add_permissions is not None:
        if len(args.add_permissions) < 1:
            filenames = input('App file name(s): ')
        else:
            filenames = args.add_permissions[0].split(',')

        if len(args.add_permissions) < 2:
            usernames = input('User name(s): ')
            
        else:
            usernames = args.add_permissions[1].split(',')

        add_permissions(filenames, usernames)

        
    if args.list_users:
        list_users()

    if args.list_apps:
        list_apps()

    if args.rm_app is not None:
        rm_app(args.rm_app[0])

    if args.rm_user is not None:
        rm_user(args.rm_user[0])

    if args.rm_permissions is not None:
        filenames = args.rm_permissions[0].split(',')
        usernames = args.rm_permissions[1].split(',')
        
        rm_permissions(filenames, usernames)

    
          
if __name__ == "__main__":
    main()
