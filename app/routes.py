from flask import render_template, request, url_for, redirect, session, flash, abort
from flask_login import login_user, login_required, current_user, logout_user
from app.models import User
from app import login_manager, app
from app.forms import LoginForm

def is_safe_url(target):
    '''
    This needs to be implemented properly
    '''
    return True


@login_manager.user_loader
def load_user(id):
    return User.query.get(int(id))

@app.route('/view')
@login_required
def view():
    appId = request.args['id']
    currentApp = current_user.apps.filter_by(id=appId).first()
    return render_template('view.html', url=currentApp.url)
    

@app.route('/billing')
@login_required
def index():
    return render_template('billing.html')

@app.route('/dashboard')
@login_required
def dashboard():
    apps=current_user.apps.all()
    return render_template('dashboard.html', apps=apps)

@app.route('/login', methods=['GET','POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()

    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data.lower()).first()

        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))

        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('dashboard'))

    return render_template('login.html', title='Sign in', form=form)

@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))
