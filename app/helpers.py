from functools import wraps

from flask import Flask, current_app, session
from flask_login import login_required, current_user, LoginManager
from flask_sqlalchemy import SQLAlchemy

from dash import Dash


import os, sys
sys.path.append('../')
from jotun_secrets import secret_key

from app.models import User, App
from app import login_manager
from app.config import Config

def valid_user_required(func):

    @wraps(func)
    def decorated_view(*args, **kwargs):
        print(session)
        return func(*args, **kwargs)

    return decorated_view

class SecureFlask(Flask):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.secret_key = secret_key
        self.login_manager = LoginManager(self)
        self.config.from_object(Config)
        self.config.update(SESSION_COOKIE_NAME='session')

        self.db = SQLAlchemy(self)
        
        @self.login_manager.user_loader
        def load_user(id):
            return User.query.get(int(id))
        
    
    def current_user_valid_for_app(self):
        fileName = os.path.basename(self.name)\
                          .replace('uwsgi_file_','')
        print('Given file name: %s'%fileName)
        currentApp = App.query.filter_by(filename=fileName).first()
        
        if currentApp is None:
            raise ValueError('currentApp should not be None.')
            return False
        
        userApps = [app.id for app in current_user.apps.all()]

        return currentApp.id in userApps

class SecureDash(Dash):
    
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        
        self.server.secret_key = secret_key
        self.login_manager = LoginManager(self.server)
        self.server.config.from_object(Config)
        self.server.config.update(SESSION_COOKIE_NAME='session')

        self.db = SQLAlchemy(self.server)
        
        @self.login_manager.user_loader
        def load_user(id):
            return User.query.get(int(id))

    def protect(self):
        for view_func in self.server.view_functions:
            if view_func.startswith(self.url_base_pathname):
                self.server.view_functions[view_func] = login_required(self.server.view_functions[view_func])        

