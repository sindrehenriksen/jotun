from flask_login import UserMixin
from werkzeug.security import check_password_hash, generate_password_hash

from app import db

appUser = db.Table('app_user',
                   db.Column('app_id', db.ForeignKey('app.id'), primary_key=True),
                   db.Column('user_id', db.ForeignKey('user.id'), primary_key=True)
)

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    apps = db.relationship('App', secondary=appUser,
                           backref='user', lazy='dynamic')

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)
        
    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return '<User {}>'.format(self.username)
    

class App(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(64), unique=True)
    filename = db.Column(db.String(64), unique=True)
    description = db.Column(db.String(180))
    
    def __repr__(self):
        return '<App {}>'.format(self.id)

