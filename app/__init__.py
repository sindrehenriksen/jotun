from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy

import sys

sys.path.append('../')

from jotun_secrets import secret_key

from app.config import Config


'''
-------------------------------------
App configuration and initialisation
-------------------------------------
'''

app = Flask(__name__)

app.secret_key = secret_key
app.config.from_object(Config)
app.config.update(SESSION_COOKIE_NAME='session')

login_manager = LoginManager(app)
login_manager.login_view = 'login'

db = SQLAlchemy(app)

from app import routes

if __name__ == "__main__":
    app.run(host='0.0.0.0',debug=True)
